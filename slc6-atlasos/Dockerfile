# SLC6 OS capable of using/running the ATLAS software release(s).

# Make the base image configurable:
ARG BASEIMAGE=cern/slc6-base:latest

# Set up the SLC6 "ATLAS OS":
FROM ${BASEIMAGE}

# Perform the installation as root:
USER root
WORKDIR /root

# Put the repository configuration file(s) in place:
COPY *.repo /etc/yum.repos.d/
# Copy the prompt setup script in place:
COPY atlas_prompt.sh /etc/profile.d/

# 1. Install extra SLC6 and LCG packages needed by the ATLAS release
# 2. Fix up the GCC setup scripts from the problematic RPMs provided by LCG
# 3. Add some soft links to the GCC installation for backwards compatibility
# 4. Install CMake
RUN yum -y install which git wget tar atlas-devel \
      redhat-lsb-core libX11-devel libXpm-devel libXft-devel libXext-devel \
      openssl-devel glibc-devel rpm-build nano sudo \
      gcc_6.2binutils_x86_64_slc6_gcc62_opt binutils_2.28_x86_64_slc6 \
    && yum clean all \
    && sed -i.bak -e 's:/cvmfs/sft.cern.ch/lcg/contrib/bintuils:/opt/lcg/binutils:g' \
       /opt/lcg/gcc/6.2binutils/x86_64-slc6-gcc62-opt/setup.sh \
       /opt/lcg/gcc/6.2binutils/x86_64-slc6-gcc62-opt/setup.csh \
    && rm -f /opt/lcg/gcc/6.2binutils/x86_64-slc6-gcc62-opt/setup.sh.bak \
             /opt/lcg/gcc/6.2binutils/x86_64-slc6-gcc62-opt/setup.sh~ \
             /opt/lcg/gcc/6.2binutils/x86_64-slc6-gcc62-opt/setup.csh.bak \
             /opt/lcg/gcc/6.2binutils/x86_64-slc6-gcc62-opt/setup.csh~ \
    && ln -s 6.2binutils /opt/lcg/gcc/6.2.0binutils \
    && ln -s x86_64-slc6-gcc62-opt /opt/lcg/gcc/6.2binutils/x86_64-slc6 \
    && wget https://cmake.org/files/v3.11/cmake-3.11.1-Linux-x86_64.tar.gz \
    && tar -C /usr/local --strip-components=1 --no-same-owner \
      -xvf cmake-3.11.1-Linux-x86_64.tar.gz \
    && rm cmake-3.11.1-Linux-x86_64.tar.gz

# Set up the ATLAS user, and give it super user rights.
RUN echo '%wheel	ALL=(ALL)	NOPASSWD: ALL' >> /etc/sudoers && \
    adduser atlas && \
    usermod -aG wheel atlas

# LH: Make Images Grid / Singularity compatible
# Note that this is a hack as long as overlayfs
# is not widely on grid sites using singularity
# as a container runtime. For overlay-enabled
# runtimes (such as docker) missing mountpoints
# are created on demand. I hope we can remove this soon.
RUN mkdir -p /alrb /cvmfs /afs /eos

# Add basic usage instructions for the image:
COPY motd /etc/

# Start the image with BASH by default, after having printed the message
# of the day.
CMD cat /etc/motd && /bin/bash
